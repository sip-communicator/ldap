/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.slick.ldap;

import java.util.*;

import org.osgi.framework.*;
import junit.framework.*;

import net.java.sip.communicator.util.*;

import net.java.sip.communicator.service.ldap.*;
import net.java.sip.communicator.service.ldap.event.*;

/**
 * Tests LdapService behaviour.
 *
 * @author Sébastien
 */
public class TestLdapService
    extends TestCase
    implements LdapConstants,
               LdapListener
{
    private Logger logger = Logger.getLogger(getClass().getName());

    /**
     * The LdapService that we will be testing.
     */
    private LdapService ldapService = null;

    /**
     * Generic JUnit Constructor.
     * @param name the name of the test
     */
    public TestLdapService(String name)
    {
        super(name);
        BundleContext context = LdapServiceLick.bc;
        ServiceReference ref = context.getServiceReference(
            LdapService.class.getName());
        ldapService = (LdapService)context.getService(ref);
    }

    public void ldapEventReceived(LdapEvent event)
    {
        this.logger.trace("I received an LDAP event, cause: " + event.getCause());

        switch(event.getCause())
        {
            case NEW_SEARCH_RESULT:
                this.logger.trace((LdapSearchResult) event.getContent());
                break;
        }
    }

    /**
     * Generic JUnit setUp method.
     * @throws Exception if anything goes wrong.
     */
    protected void setUp() throws Exception
    {
        super.setUp();
    }

    /**
     * Generic JUnit tearDown method.
     * @throws Exception if anything goes wrong.
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    public void test00ServerCreation00()
    {
        try
        {
        LdapServer server =
            ldapService.createLdapServer("  ", "example.com", 389,
                    "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);
        fail("createLdapServer accepted an empty alias argument!");
        }
        catch(IllegalArgumentException e)
        {
            assertNotNull(e.getMessage());
        }
    }

    public void test00ServerCreation01()
    {
        try
        {
        LdapServer server =
            ldapService.createLdapServer(null, "example.com", 389,
                    "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);
        fail("createLdapServer accepted a null alias argument!");
        }
        catch(IllegalArgumentException e)
        {
            assertNotNull(e.getMessage());
        }
    }

    public void test00ServerCreation02()
    {
        try
        {
        LdapServer server =
            ldapService.createLdapServer("my server", "  ", 389,
                    "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);
        fail("createLdapServer accepted an empty host argument!");
        }
        catch(IllegalArgumentException e)
        {
            assertNotNull(e.getMessage());
        }
    }

    public void test00ServerCreation03()
    {
        try
        {
        LdapServer server =
            ldapService.createLdapServer("my server", null, 389,
                    "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);
        fail("createLdapServer accepted a null host argument!");
        }
        catch(IllegalArgumentException e)
        {
            assertNotNull(e.getMessage());
        }
    }

    public void test00ServerCreation04()
    {
        try
        {
        LdapServer server =
            ldapService.createLdapServer("my server", "example.com", -50,
                    "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);
        fail("createLdapServer accepted a negative port argument!");
        }
        catch(IllegalArgumentException e)
        {
            assertNotNull(e.getMessage());
        }
    }

    /* port = 0 to set default port */
    public void test00ServerCreation05()
    {
        LdapServer server =
            ldapService.createLdapServer("my server", "example.com", 0,
                    "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);
        assertEquals("default port setting cannot be retrieved" ,0 , server.getPort());
    }

    public void test00ServerCreation06()
    {
        try
        {
            LdapServer server =
                ldapService.createLdapServer("my server", "example.com", 65536,
                        "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                        "secret", ConnectMethod.SIMPLE);
            fail("createLdapServer accepted an invalid port number!");
        }
        catch(IllegalArgumentException e)
        {
            assertNotNull(e.getMessage());
        }
    }

    public void test00ServerCreation07()
    {
        try
        {
        LdapServer server =
            ldapService.createLdapServer("my server", "example.com", 389,
                    null, "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);
        fail("createLdapServer accepted a null base DN!");
        }
        catch(IllegalArgumentException e)
        {
            assertNotNull(e.getMessage());
        }
    }

    public void test00ServerCreation08()
    {
        try
        {
        LdapServer server =
            ldapService.createLdapServer("my server", "example.com", 389,
                    "  ", "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);
        fail("createLdapServer accepted an empty base DN!");
        }
        catch(IllegalArgumentException e)
        {
            assertNotNull(e.getMessage());
        }
    }

    public void test00ServerCreation09()
    {
        try
        {
        LdapServer server =
            ldapService.createLdapServer("my server", "example.com", 389,
                    "dc=example,dc=com", null,
                    "secret", ConnectMethod.SIMPLE);
        fail("createLdapServer accepted a null bind DN!");
        }
        catch(IllegalArgumentException e)
        {
            assertNotNull(e.getMessage());
        }
    }

    public void test00ServerCreation10()
    {
        try
        {
        LdapServer server =
            ldapService.createLdapServer("my server", "example.com", 389,
                    "dc=example,dc=com", "  ",
                    "secret", ConnectMethod.SIMPLE);
        fail("createLdapServer accepted an empty bind DN!");
        }
        catch(IllegalArgumentException e)
        {
            assertNotNull(e.getMessage());
        }
    }

    public void test00ServerCreation11()
    {
        try
        {
        LdapServer server =
            ldapService.createLdapServer("my server", "example.com", 389,
                    "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                    null, ConnectMethod.SIMPLE);
        fail("createLdapServer accepted a null password!");
        }
        catch(IllegalArgumentException e)
        {
            assertNotNull(e.getMessage());
        }
    }

    public void test00ServerCreation12()
    {
        LdapServer server =
            ldapService.createLdapServer("my server", "example.com", 389,
                    "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);
    }

    public void test01ServerGetters00()
    {
        LdapServer server =
            ldapService.createLdapServer("my server", "example.com", 389,
                    "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);

        assertEquals("getName failed", "my server", server.getName());
    }

    public void test01ServerGetters01()
    {
        LdapServer server =
            ldapService.createLdapServer("my server", "example.com", 389,
                    "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);

        assertEquals("getHostname failed", "example.com", server.getHostname());
    }

    public void test01ServerGetters02()
    {
        LdapServer server =
            ldapService.createLdapServer("my server", "example.com", 389,
                    "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);

        assertEquals("getPort failed", 389, server.getPort());
    }

    public void test01ServerGetters03()
    {
        LdapServer server =
            ldapService.createLdapServer("my server", "example.com", 389,
                    "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);

        assertEquals("getBaseDN failed", "dc=example,dc=com", server.getBaseDN());
    }

    public void test01ServerGetters04()
    {
        LdapServer server =
            ldapService.createLdapServer("my server", "example.com", 389,
                    "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);

        assertEquals("getBindDN failed", "cn=myself,dc=example,dc=com", server.getBindDN());
    }

    public void test01ServerGetters05()
    {
        LdapServer server =
            ldapService.createLdapServer("my server", "example.com", 389,
                    "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);

        assertEquals("getPassword failed", "secret", server.getPassword());
    }

    public void test01ServerGetters06()
    {
        LdapServer server =
            ldapService.createLdapServer("my server", "example.com", 389,
                    "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);

        assertEquals("getConnecteMethod failed", ConnectMethod.SIMPLE, server.getConnectMethod());
    }

    public void test02ServerSet00()
    {
        int i;

        /* test server set creation */
        LdapServerSet serverSet = this.ldapService.getServerSet();
        if(serverSet.size() != 0)
        {
            fail("server set should contain 0 element");
        }
        i = 0;
        for(LdapServer server : serverSet)
        {
            fail("0 iteration expected");
        }

        /* test simple insertion */
        LdapServer server0 =
            this.ldapService.createLdapServer("my server", "example.com", 389,
                    "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);
        if(!serverSet.addServer(server0))
        {
            fail("simple insertion in the server set failed!");
        }
        assertEquals("server set should contain 1 element", 1, serverSet.size());
        i = 0;
        for(LdapServer server : serverSet)
        {
            if(i == 0 && !server.getName().equals("my server"))
            {
                fail("\"my server\" expected as first element");
            }
            if(i > 0)
            {
                fail("1 iteration expected");
            }
            i++;
        }

        /* test duplicate insertion */
        assertFalse("duplicate insertion in the server set!", serverSet.addServer(server0));
        assertEquals("server set should contain 1 element", 1, serverSet.size());
        i = 0;
        for(LdapServer server : serverSet)
        {
            if(i == 0 && !server.getName().equals("my server"))
            {
                fail("\"my server\" expected as first element");
            }
            if(i > 0)
            {
                fail("1 iteration expected");
            }
            i++;
        }

        /* test second element insertion and servers order */
        LdapServer server1 =
            this.ldapService.createLdapServer("my other server", "example.com", 389,
                    "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);
        assertTrue("new insertion in the server set failed!", serverSet.addServer(server1));
        assertEquals("server set should contain 2 elements", 2, serverSet.size());
        i = 0;
        for(LdapServer server : serverSet)
        {
            if(i == 0 && !server.getName().equals("my other server"))
            {
                fail("\"my other server\" expected as first element");
            }
            if(i == 1 && !server.getName().equals("my server"))
            {
                fail("\"my server\" expected as second element");
            }
            if(i > 1)
            {
                fail("2 iterations expected");
            }
            i++;
        }


        /* test remove */
        LdapServer server2 =
            this.ldapService.createLdapServer("my first server", "example.com", 389,
                    "dc=example,dc=com", "cn=myself,dc=example,dc=com",
                    "secret", ConnectMethod.SIMPLE);
        assertEquals("removeServerWithName failed", "my other server", serverSet.removeServerWithName("my other server").getName());
        assertEquals("server set should contain 1 element", 1, serverSet.size());
        i = 0;
        for(LdapServer server : serverSet)
        {
            if(i == 0 && !server.getName().equals("my server"))
            {
                fail("\"my server\" expected as first element");
            }
            if(i > 0)
            {
                fail("1 iteration expected");
            }
            i++;
        }

        /* test getServerWithName */
        assertEquals("getServerWithName failed", "my server", serverSet.getServerWithName("my server").getName());
        assertNull("shouldn't have found \"my other server\"", serverSet.getServerWithName("my other server"));

    }

    public void test03ServerSearch00()
    {
        LdapServer server =
            ldapService.createLdapServer("sandbox test server", "cambuse.org", 389,
                    "ou=Mickey Mouse Universe,dc=sandbox,dc=org", ConnectMethod.SIMPLE);

        server.addLdapListener(this);
        server.searchContact("duck");
        /* give time for events to be received */
        try{
            Thread.sleep(2000);
        }
        catch(Exception e){}
    }
}
