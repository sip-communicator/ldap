
/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.plugin.ldaptools;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

import net.java.sip.communicator.service.contactlist.*; /* MetaContact     */
import net.java.sip.communicator.service.gui.*;         /* PluginComponent */

/**
 * The LdapMenuItem is a tools menu item.
 *
 * @author Sébastien Mazy
 */
public class LdapToolsComponent
    implements ActionListener,
               PluginComponent
{

    private JMenuItem ldapMenuItem = null;
    private LdapConfigDialog ldapConfigDialog = null;

    public LdapToolsComponent()
    {
        this.ldapMenuItem = new JMenuItem("Experimental LDAP tool");
        this.ldapMenuItem.addActionListener(this);        
    }


    public void actionPerformed(ActionEvent e)
    {
        if(this.ldapConfigDialog == null)
        {
            this.ldapConfigDialog = new LdapConfigDialog();

            this.ldapConfigDialog.setLocation(
                    Toolkit.getDefaultToolkit().getScreenSize().width/2
                    - ldapConfigDialog.getWidth()/2,
                    Toolkit.getDefaultToolkit().getScreenSize().height/2
                    - ldapConfigDialog.getHeight()/2
                    );

        }

        ldapConfigDialog.setVisible(true);
    }

    public Object getComponent()
    {
        return this.ldapMenuItem;
    }

    public String getConstraints()
    {
        return null;
    }

    public Container getContainer()
    {
        return Container.CONTAINER_TOOLS_MENU;
    }

    public String getName()
    {
        return this.ldapMenuItem.getText();
    }

    public void setCurrentContact(MetaContact metaContact)
    {
    }

    public void setCurrentContactGroup(MetaContactGroup metaGroup)
    {
    }

    public int getPositionIndex()
    {
        return -1;
    }
}
