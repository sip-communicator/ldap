/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.plugin.ldaptools;

import java.util.*;

import net.java.sip.communicator.util.*;
import net.java.sip.communicator.service.gui.*;
import net.java.sip.communicator.service.ldap.*;
//import net.java.sip.communicator.plugin.ldaptools.wizard.*;

import org.osgi.framework.*;

public class LdapToolsActivator implements BundleActivator
{
    /**
     * The logger for this class.
     */
    private static Logger logger =
        Logger.getLogger(LdapToolsActivator.class);

    /**
     * BundleContext for this class,
     * needed by getLdapService()
     */
    private static BundleContext bundleContext;

    /**
     * Starts the LDAP tools plugin.
     */
    public void start(BundleContext bc) throws Exception
    {   
        bundleContext = bc;

        ServiceReference uiServiceRef
            = bundleContext.getServiceReference(UIService.class.getName());

        UIService uiService
            = (UIService) bundleContext.getService(uiServiceRef);        

        /*LdapToolsComponent ldapToolsComponent
            = new LdapToolsComponent();*/

        LdapConfigForm configForm = new LdapConfigForm();
        bundleContext.registerService(ConfigurationForm.class.getName(),
                configForm,
                null);


        logger.trace("Starting LDAP plugin...");

        /* registers the menu item */
        /*
        Hashtable<String, String> containerFilter
            = new Hashtable<String, String>();
        containerFilter.put(Container.CONTAINER_ID,
                Container.CONTAINER_TOOLS_MENU.getID());
        bundleContext.registerService(PluginComponent.class.getName(),
                ldapToolsComponent,
                containerFilter);
         */
    }

    /**
     * Stops the LDAP tools plugin.
     */
    public void stop(BundleContext bc) throws Exception
    {
    }    


    /**
     * Returns an instance of LdapService.
     */
    public static LdapService getLdapService()
    {
        ServiceReference ldapServiceRef
            = bundleContext.getServiceReference(LdapService.class.getName());
        LdapService ldapService
            = (LdapService) bundleContext.getService(ldapServiceRef);
        return ldapService;
    }
}
