/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.plugin.ldaptools;

import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

import net.java.sip.communicator.util.*;
import net.java.sip.communicator.service.gui.*;
import net.java.sip.communicator.service.ldap.*;
import net.java.sip.communicator.plugin.ldaptools.*;
import net.java.sip.communicator.plugin.ldaptools.i18n.*;
import net.java.sip.communicator.plugin.ldaptools.wizard.*;
import net.java.sip.communicator.plugin.ldaptools.wizard.panels.*;

import org.osgi.framework.*;

/**
 * This ConfigurationForm shows the list of LDAP directories
 * and allow users to manage them.
 * 
 * @author Sébastien Mazy
 */
public class LdapConfigForm
    extends JPanel
    implements ConfigurationForm, ActionListener
{
    /**
     * The logger for this class.
     */
    private static Logger logger = Logger.getLogger(LdapConfigForm.class);

    /**
     * opens the new directory registration wizard
     */
    private JButton newButton = new JButton("New");

    /**
     * opens a directory modification dialog
     */
    private JButton modifyButton = new JButton("Modify");

    /**
     * pops a directory deletion confirmation dialog
     */
    private JButton removeButton = new JButton("Remove");

    /**
     * displays the registered directories
     */
    private JTable directoryTable = new JTable();

    /**
     * contains the new/modify/remove buttons
     */
    private JPanel buttonsPanel = new JPanel(new GridLayout(0, 1, 8, 8));

    /**
     * contains the directoryTable
     */
    private JPanel listPanel = new JPanel(new BorderLayout());

    /**
     * contains the buttonsPanel,
     */
    private JPanel rightPanel = new JPanel(new BorderLayout());

    /**
     * contains listPanel and rightPanel
     */
    private JPanel mainPanel = this;

    /**
     * the ldap new/edit dialog where users
     * can modify per server settings
     */
    private LdapEditDialog editDialog = null;

    /**
     * the LdapService
     */
    private LdapService serviceRef = LdapToolsActivator.getLdapService();

    /**
     * the set of registered servers
     */
    private LdapServerSet serverSet = this.serviceRef.getServerSet();

    /**
     * model for the directoryTable
     */
    private LdapTableModel tableModel = new LdapTableModel(serverSet);

    /**
     * wizard to register a new directory
     */
    private Wizard wizard = null;

    /**
     * panel descriptors
     */
    private WizardPanelDescriptor descriptor0 = new IntroductionPanelDescriptor();
    private WizardPanelDescriptor descriptor1 = new ConnectionPanelDescriptor();
    private WizardPanelDescriptor descriptor2 = new AuthPanelDescriptor();
    private WizardPanelDescriptor descriptor3 = new SearchPanelDescriptor();
    private WizardPanelDescriptor descriptor4 = new SummaryPanelDescriptor();

    /**
     * Constructor
     */
    public LdapConfigForm()
    {
        super(new BorderLayout());

        this.buildUI();
    }

    private void buildUI()
    {
        this.directoryTable.setRowHeight(22);
        this.directoryTable.setSelectionMode(
                ListSelectionModel.SINGLE_SELECTION);

        this.directoryTable.setShowHorizontalLines(false);
        this.directoryTable.setShowVerticalLines(false);
        this.directoryTable.setModel(tableModel);  
        this.directoryTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);

        this.newButton.addActionListener(this);
        this.modifyButton.addActionListener(this);
        this.removeButton.addActionListener(this);

        this.buttonsPanel.add(this.newButton);
        this.buttonsPanel.add(this.modifyButton);
        this.buttonsPanel.add(this.removeButton);

        /* consistency with the accounts config form */
        this.rightPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        this.rightPanel.add(this.buttonsPanel, BorderLayout.NORTH); 

        this.listPanel.add(this.directoryTable);
        this.mainPanel.add(this.listPanel, BorderLayout.CENTER); 
        this.mainPanel.add(this.rightPanel, BorderLayout.EAST); 

        this.mainPanel.setPreferredSize(new Dimension(500, 400));

    }

    /**
     * @see net.java.sip.communicator.service.gui.ConfigurationForm#getTitle
     */
    public String getTitle()
    {
        return Resources.getString("configTitle");
    }

    /**
     * @see net.java.sip.communicator.service.gui.ConfigurationForm#getIcon
     */
    public byte[] getIcon()
    {
        return Resources.getImageInBytes("icon");
    }

    /**
     * @see net.java.sip.communicator.service.gui.ConfigurationForm#getForm
     */
    public Object getForm()
    {
        return this;
    }

    /**
     * Processes button events
     *
     * @see java.awt.event.ActionListener#actionPerformed
     */
    public void actionPerformed(ActionEvent e)
    {
        Object source = e.getSource();
        int row = this.directoryTable.getSelectedRow();

        if (source == this.newButton)
        {   
            //this.editDialog = new LdapEditDialog(this.serverSet, null);


            if(this.wizard == null)
            {
                this.wizard = new Wizard();

                wizard.getDialog().setTitle("Test Wizard Dialog");

                this.wizard.registerWizardPanel(IntroductionPanelDescriptor.IDENTIFIER, this.descriptor0);
                this.wizard.registerWizardPanel(ConnectionPanelDescriptor.IDENTIFIER, this.descriptor1);
                this.wizard.registerWizardPanel(AuthPanelDescriptor.IDENTIFIER, this.descriptor2);
                this.wizard.registerWizardPanel(SearchPanelDescriptor.IDENTIFIER, this.descriptor3);
                this.wizard.registerWizardPanel(SummaryPanelDescriptor.IDENTIFIER, this.descriptor4);
            }

            wizard.setCurrentPanel(IntroductionPanelDescriptor.IDENTIFIER);

            int ret = wizard.showModalDialog();
        }
        else if (source == this.modifyButton && row != -1)
        { 
            LdapServer server = this.tableModel.getServerAt(row);
            this.editDialog = new LdapEditDialog(server, null);

        }
        else if (source == this.removeButton && row != -1)
        {
            this.serverSet.removeServerWithName(this.tableModel.getServerAt(row).toString());
        }
    }
}
