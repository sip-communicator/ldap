/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */

package net.java.sip.communicator.plugin.ldaptools;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import net.java.sip.communicator.service.ldap.*;
import net.java.sip.communicator.service.ldap.event.*;


/**
 * The LdapConfigDialog allows to configure
 * LDAP connexion settings.
 * 
 * @author Sébastien Mazy
 */
public class LdapEditDialog
    extends JDialog
    implements ActionListener,
               ItemListener,
               LdapConstants
{

    /*
     * The LdapServer we're modifying or creating
     */
    private LdapServer server;

    /*
     * The LdapServerSet
     */
    private LdapServerSet serverSet;

    /* Constants */
    private static final String connectionStateText
        = "Connection state: ";
    private static final String connectionErrorText
        = "Last error: ";


    /* UI components */
    private JPanel mainPanel = new JPanel();
    private JPanel labelPanel = new JPanel();
    private JPanel textFieldPanel = new JPanel();
    private JPanel buttonPanel = new JPanel();

    private JLabel nameLabel = new JLabel("Name:");
    private JLabel hostnameLabel = new JLabel("Hostname:");
    private JLabel baseDNLabel = new JLabel("Base DN:");
    private JLabel portNumberLabel = new JLabel("Port number:");
    private JLabel bindDNLabel = new JLabel("Bind DN:");
    private JLabel passwordLabel = new JLabel("Bind password:");

    private JTextField aliasTextField = new JTextField(20);
    private JTextField hostnameTextField = new JTextField(20);
    private JTextField baseDNTextField = new JTextField(20);
    private JTextField portNumberTextField = new JTextField(5);
    private JTextField bindDNTextField = new JTextField(20);

    private JPasswordField passwordField = new JPasswordField(20);

    private JCheckBox sslCheckBox = new JCheckBox("Use SSL");
    private JCheckBox anonymousCheckBox = new JCheckBox("Anonymous bind");

    private JButton cancelButton = new JButton("Cancel");
    private JButton saveButton = new JButton("Save");

    private JTextPane connectionStatePane = new JTextPane();
    private JTextPane connectionErrorPane = new JTextPane();

    private GridBagConstraints constraints = new GridBagConstraints();

    public String getHostname()
    {
        return this.hostnameTextField.getText();
    }

    /**
     * Constructor,
     * Opens a new dialog for creating a new server entry
     */
    public LdapEditDialog(LdapServerSet serverSet, Frame owner)
    {
        this.serverSet = serverSet;
        this.server = null;

        this.initUI();
    }

    /**
     * Constructor,
     * Opens a new dialog for modifying an existing server entry
     */
    public LdapEditDialog(LdapServer server, Frame owner)
    {
        this.serverSet = null;
        this.server = server;

        this.initUI();

    }


    /**
     * Set the values of the components of the dialog, depending
     * on whether it is a server creation or modification
     */
    private void initValues()
    {
        // creation dialog
        if(server == null)
        {
            this.bindDNTextField.setText("bind distinguished name");
            this.bindDNTextField.setEnabled(false);
            this.passwordField.setEnabled(false);
            this.aliasTextField.setText("default");
            this.hostnameTextField.setText("example.com");
            this.baseDNTextField.setText("base distinguished name");
            this.portNumberTextField.setText("389");
            this.sslCheckBox.setSelected(false);
            this.anonymousCheckBox.setSelected(false);
        }
        else //modification dialog
        {
            this.aliasTextField.setText(this.server.toString());
            this.hostnameTextField.setText(this.server.getHostname());
            this.baseDNTextField.setText(this.server.getBaseDN());
            this.portNumberTextField.setText(String.valueOf(this.server.getPort()));
            this.bindDNTextField.setText(this.server.getBindDN());
            this.passwordField.setText(String.valueOf(this.server.getPassword()));
        }
    }

    /**
     * Initializes UI components
     */
    private void initUI()
    {
        this.setTitle("Edit LDAP server");
        this.setModal(true);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setContentPane(mainPanel);
        this.mainPanel.setLayout(new GridBagLayout());

        this.initValues();

        /* default values */
        /* TODO query connection state at init */
        this.connectionStatePane.setText(connectionStateText + "undefined");
        this.connectionErrorPane.setText(connectionErrorText + "none");
        this.connectionStatePane.setEditable(false);
        this.getRootPane().setDefaultButton(this.saveButton);

        /* common values for some components */
        this.constraints.insets = new Insets(5, 5, 5, 5);
        this.constraints.fill = GridBagConstraints.HORIZONTAL;
        this.constraints.anchor = GridBagConstraints.WEST;

        /* Name */
        this.constraints.gridy = 0;
        this.constraints.gridx = 0;
        this.constraints.gridwidth = 1;
        this.mainPanel.add(this.nameLabel, this.constraints);
        this.constraints.gridx = 1;
        this.constraints.gridwidth = 3;
        this.mainPanel.add(this.aliasTextField, this.constraints);

        /* Hostname */
        this.constraints.gridy = 1;
        this.constraints.gridx = 0;
        this.constraints.gridwidth = 1;
        this.mainPanel.add(this.hostnameLabel, this.constraints);
        this.constraints.gridx = 1;
        this.constraints.gridwidth = 3;
        this.mainPanel.add(this.hostnameTextField, this.constraints);

        /* Base DN */
        this.constraints.gridy = 2;
        this.constraints.gridx = 0;
        this.constraints.gridwidth = 1;
        this.mainPanel.add(this.baseDNLabel, this.constraints);
        this.constraints.gridx = 1;
        this.constraints.gridwidth = 3;
        this.mainPanel.add(this.baseDNTextField, this.constraints);

        /* Port number */
        this.constraints.gridy = 3;
        this.constraints.gridx = 0;
        this.constraints.gridwidth = 1;
        this.mainPanel.add(this.portNumberLabel, this.constraints);
        this.constraints.gridx = 1;
        this.mainPanel.add(this.portNumberTextField, this.constraints);

        /* SSL check box */
        this.constraints.gridx = 2;
        this.mainPanel.add(this.sslCheckBox, this.constraints);

        /* SSL check box */
        this.constraints.gridy = 4;
        this.constraints.gridx = 0;
        this.constraints.gridwidth = 4;
        this.mainPanel.add(this.anonymousCheckBox, this.constraints);

        /* Bind DN */
        this.constraints.gridy = 5;
        this.constraints.gridx = 0;
        this.constraints.gridwidth = 1;
        this.mainPanel.add(this.bindDNLabel, this.constraints);
        this.constraints.gridx = 1;
        this.constraints.gridwidth = 3;
        this.mainPanel.add(this.bindDNTextField, this.constraints);

        /* Password */
        this.constraints.gridy = 6;
        this.constraints.gridx = 0;
        this.constraints.gridwidth = 1;
        this.mainPanel.add(this.passwordLabel, this.constraints);
        this.constraints.gridx = 1;
        this.constraints.gridwidth = 3;
        this.mainPanel.add(this.passwordField, this.constraints);

        /* State text pane */
        this.constraints.gridy = 7;
        this.constraints.gridx = 0;
        this.constraints.gridwidth = 4;
        this.mainPanel.add(this.connectionStatePane, this.constraints);

        /* State text pane */
        this.constraints.gridy = 8;
        this.constraints.gridx = 0;
        this.constraints.gridwidth = 4;
        this.mainPanel.add(this.connectionErrorPane, this.constraints);

        /* Buttons */
        this.constraints.anchor = GridBagConstraints.EAST;
        this.constraints.gridy = 9;
        this.constraints.gridx = 2;
        this.constraints.gridwidth = 1;
        this.mainPanel.add(this.cancelButton, this.constraints);
        this.constraints.gridx = 3;
        this.mainPanel.add(this.saveButton, this.constraints);

        /* listeners */
        this.sslCheckBox.addItemListener(this);
        this.anonymousCheckBox.addItemListener(this);
        this.cancelButton.addActionListener(this);
        this.saveButton.addActionListener(this);

        /* let's display it */
        this.pack();
        this.setVisible(true);
    }


    /**
     * Processes button events
     */
    public void actionPerformed(ActionEvent e)
    {
        Object source = e.getSource();

        if (source == this.cancelButton)
        {   
            this.dispose();
        }
        else if (source == this.saveButton)
        { 
            String alias = this.aliasTextField.getText();
            String hostname = this.hostnameTextField.getText();
            int port = Integer.parseInt(this.portNumberTextField.getText());
            String baseDN = this.baseDNTextField.getText();
            String bindDN = null;
            char[] password = {};
            if(anonymousCheckBox.isSelected() == false)
            {
                bindDN = this.bindDNTextField.getText();
                password = this.passwordField.getPassword();
            }
            ConnectMethod method;
            if(sslCheckBox.isSelected())
            {
                //method = LDAP_METHOD_SSL;
                method = ConnectMethod.SSL;
            }
            else
            {
                method = ConnectMethod.SIMPLE;
            }
            //modification
            if(this.server != null)
            {
                /*
                this.server.modifify(alias, hostname, port, baseDN,
                        bindDN, password, method);
                        */
            }
            else //creation
            {
                /*
                this.serverSet.add(alias, hostname, port, baseDN,
                        bindDN, password, method);
                        */
            }
            this.dispose();
        }
    }

    public void itemStateChanged(ItemEvent e) {

        Object source = e.getItemSelectable();

        if (source == this.sslCheckBox)
            if (e.getStateChange() == ItemEvent.DESELECTED)
                this.portNumberTextField.setText("389");
            else
                this.portNumberTextField.setText("636");
        else if (source == this.anonymousCheckBox)
        {
            this.bindDNTextField.setEnabled(e.getStateChange() == ItemEvent.DESELECTED);
            this.passwordField.setEnabled(e.getStateChange() == ItemEvent.DESELECTED);
        }
    }

    /**
     * Processes LDAP connection state change events.
     */
    public void ldapStateChanged(LdapEvent event)
    {
        /* that dirty implementation will be replaced
         * when i18n will be implemented
         */

        /* state messages */
        if(event.getNewState() == event.LDAP_STATE_DISCONNECTED)
        {
            this.connectionStatePane.setText(connectionStateText + "disconnected");
        }
        else if(event.getNewState() == event.LDAP_STATE_CONNECTED)
        {
            this.connectionStatePane.setText(connectionStateText + "connected");

        }
        else if(event.getNewState() == event.LDAP_STATE_DISCONNECTED)
        {

        }
        else if(event.getNewState() == event.LDAP_STATE_CONNECTING)
        {
            this.connectionStatePane.setText(connectionStateText + "connecting...");
        }
        else if(event.getNewState() == event.LDAP_STATE_DISCONNECTING)
        {
            this.connectionStatePane.setText(connectionStateText + "disconnecting...");
        }
        else
        {
            this.connectionStatePane.setText(connectionStateText + "unexpected state");
        }

        /* error messages */
        if(event.getError() == event.LDAP_ERROR_NONE)
        {
            this.connectionErrorPane.setText(connectionErrorText + "none");
        }
        else if(event.getError() == event.LDAP_ERROR_CONNECT)
        {
            this.connectionErrorPane.setText(connectionErrorText + "couldn't establish LDAP connection");
        }
        else if(event.getError() == event.LDAP_ERROR_AUTH)
        {
            this.connectionErrorPane.setText(connectionErrorText + "wrong credentials!");
        }
        else if(event.getError() == event.LDAP_INVALID_DN)
        {
            this.connectionErrorPane.setText(connectionErrorText + "invalid DN!");
        }
        else if(event.getError() == event.LDAP_NO_PASSWD)
        {
            this.connectionErrorPane.setText(connectionErrorText + "password needed");
        }
        else
        {
            this.connectionErrorPane.setText(connectionErrorText + "unknown error");
        }
    }


}
