/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.plugin.ldaptools;

import java.util.*;

import javax.swing.table.*;

import net.java.sip.communicator.service.ldap.*;

/**
 * A table model suitable for the directories list
 * in the config form
 *
 * @author Sébastien Mazy
 */
public class LdapTableModel
    extends AbstractTableModel
{   
    private LdapServerSet serverSet;

    public LdapTableModel(LdapServerSet serverSet)
    {
        this.serverSet = serverSet;
    }

    /**
     * @see javax.swing.table.AbstractTableModel#getColumnName
     */
    public String getColumnName(int column)
    {
        switch(column)
        {
            case 0:
                /* TODO I18N */
                return "Alias";
            case 1:
                /* TODO I18N */
                return "Hostname";
        }
        return "error";
    }


    /**
     * @see javax.swing.table.AbstractTableModel#getRowCount
     */
    public int getRowCount()
    {
        return this.serverSet.size();
    }

    /**
     * @see javax.swing.table.AbstractTableModel#getColumnCount
     */
    public int getColumnCount()
    {
        // 2 columns: "alias" and "hostname"
        return 2;
    }

    /**
     * @see javax.swing.table.AbstractTableModel#getValueAt
     */
    public Object getValueAt(int row, int column)
    {
        if(column < 0 || column > 1)
        {
            throw new IllegalArgumentException();
        }
        if(column == 0)
            return this.getServerAt(row).getName();
        else
            return this.getServerAt(row).getHostname();
    }

    public LdapServer getServerAt(int row)
    {
        /*
        if(row < 0 || row > (data.size() - 1))
        {
            throw new IllegalArgumentException();
        }
        return data.get(row);
        */
        return null;
    }

}
