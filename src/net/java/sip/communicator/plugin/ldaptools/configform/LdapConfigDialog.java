/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */

package net.java.sip.communicator.plugin.ldaptools;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import net.java.sip.communicator.service.ldap.*;
import net.java.sip.communicator.service.ldap.event.*;


/**
 * The LdapConfigDialog allows to configure
 * LDAP connexion settings.
 * 
 * @author Sébastien Mazy
 */
public class LdapConfigDialog
    extends JDialog
    implements ActionListener,
               ItemListener,
               LdapListener,
               LdapConstants
{

    /* Constants */
    private static final String connectionStateText
        = "Connection state: ";
    private static final String connectionErrorText
        = "Last error: ";


    /* UI components */
    private JPanel mainPanel = new JPanel();
    private JPanel labelPanel = new JPanel();
    private JPanel textFieldPanel = new JPanel();
    private JPanel buttonPanel = new JPanel();

    private JLabel nameLabel = new JLabel("Name:");
    private JLabel hostnameLabel = new JLabel("Hostname:");
    private JLabel baseDNLabel = new JLabel("Base DN:");
    private JLabel portNumberLabel = new JLabel("Port number:");
    private JLabel bindDNLabel = new JLabel("Bind DN:");
    private JLabel passwordLabel = new JLabel("Bind password:");

    private JTextField nameTextField = new JTextField("default",20);
    private JTextField hostnameTextField = new JTextField(20);
    private JTextField baseDNTextField = new JTextField(20);
    private JTextField portNumberTextField = new JTextField("389",5);
    private JTextField bindDNTextField = new JTextField(20);

    private JPasswordField passwordField = new JPasswordField(20);

    private JCheckBox sslCheckBox = new JCheckBox("Use SSL", false);
    private JCheckBox anonymousCheckBox = new JCheckBox("Anonymous bind", true);

    private JButton disconnectButton = new JButton("Disconnect");
    private JButton connectButton = new JButton("Connect");

    private JTextPane connectionStatePane = new JTextPane();
    private JTextPane connectionErrorPane = new JTextPane();

    private GridBagConstraints constraints = new GridBagConstraints();


    /* Service */
    private LdapService ldapService; 


    /**
     * Creates a new instance of LdapConfigDialog.
     */
    public LdapConfigDialog()
    {

        this.setContentPane(mainPanel);
        //this.setModal(true);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.initUI();
        this.pack();

        /* retrieves LDAP service implementation */
        this.ldapService = LdapToolsActivator.getLdapService();
        //this.ldapService.addLdapListener(this);
    }


    /**
     * Initializes UI components
     */
    private void initUI()
    {
        this.setTitle("LDAP configuration");
        this.mainPanel.setPreferredSize(new Dimension(400, 360));
        this.mainPanel.setLayout(new GridBagLayout());

        /* default values */
        /* TODO query connection state at init */
        this.bindDNTextField.setEnabled(false);
        this.passwordField.setEnabled(false);
        this.connectionStatePane.setText(connectionStateText + "undefined");
        this.connectionErrorPane.setText(connectionErrorText + "none");
        this.connectionStatePane.setEditable(false);
        this.getRootPane().setDefaultButton(this.connectButton);

        /* common values for some components */
        this.constraints.insets = new Insets(5, 5, 5, 5);
        this.constraints.fill = GridBagConstraints.HORIZONTAL;
        this.constraints.anchor = GridBagConstraints.WEST;

        /* Name */
        this.constraints.gridy = 0;
        this.constraints.gridx = 0;
        this.constraints.gridwidth = 1;
        this.mainPanel.add(this.nameLabel, this.constraints);
        this.constraints.gridx = 1;
        this.constraints.gridwidth = 3;
        this.mainPanel.add(this.nameTextField, this.constraints);

        /* Hostname */
        this.constraints.gridy = 1;
        this.constraints.gridx = 0;
        this.constraints.gridwidth = 1;
        this.mainPanel.add(this.hostnameLabel, this.constraints);
        this.constraints.gridx = 1;
        this.constraints.gridwidth = 3;
        this.mainPanel.add(this.hostnameTextField, this.constraints);

        /* Base DN */
        this.constraints.gridy = 2;
        this.constraints.gridx = 0;
        this.constraints.gridwidth = 1;
        this.mainPanel.add(this.baseDNLabel, this.constraints);
        this.constraints.gridx = 1;
        this.constraints.gridwidth = 3;
        this.mainPanel.add(this.baseDNTextField, this.constraints);

        /* Port number */
        this.constraints.gridy = 3;
        this.constraints.gridx = 0;
        this.constraints.gridwidth = 1;
        this.mainPanel.add(this.portNumberLabel, this.constraints);
        this.constraints.gridx = 1;
        this.mainPanel.add(this.portNumberTextField, this.constraints);

        /* SSL check box */
        this.constraints.gridx = 2;
        this.mainPanel.add(this.sslCheckBox, this.constraints);

        /* SSL check box */
        this.constraints.gridy = 4;
        this.constraints.gridx = 0;
        this.constraints.gridwidth = 4;
        this.mainPanel.add(this.anonymousCheckBox, this.constraints);

        /* Bind DN */
        this.constraints.gridy = 5;
        this.constraints.gridx = 0;
        this.constraints.gridwidth = 1;
        this.mainPanel.add(this.bindDNLabel, this.constraints);
        this.constraints.gridx = 1;
        this.constraints.gridwidth = 3;
        this.mainPanel.add(this.bindDNTextField, this.constraints);

        /* Password */
        this.constraints.gridy = 6;
        this.constraints.gridx = 0;
        this.constraints.gridwidth = 1;
        this.mainPanel.add(this.passwordLabel, this.constraints);
        this.constraints.gridx = 1;
        this.constraints.gridwidth = 3;
        this.mainPanel.add(this.passwordField, this.constraints);

        /* State text pane */
        this.constraints.gridy = 7;
        this.constraints.gridx = 0;
        this.constraints.gridwidth = 4;
        this.mainPanel.add(this.connectionStatePane, this.constraints);

        /* State text pane */
        this.constraints.gridy = 8;
        this.constraints.gridx = 0;
        this.constraints.gridwidth = 4;
        this.mainPanel.add(this.connectionErrorPane, this.constraints);

        /* Buttons */
        this.constraints.anchor = GridBagConstraints.EAST;
        this.constraints.gridy = 9;
        this.constraints.gridx = 2;
        this.constraints.gridwidth = 1;
        this.mainPanel.add(this.disconnectButton, this.constraints);
        this.constraints.gridx = 3;
        this.mainPanel.add(this.connectButton, this.constraints);

        /* listeners */
        this.sslCheckBox.addItemListener(this);
        this.anonymousCheckBox.addItemListener(this);
        this.disconnectButton.addActionListener(this);
        this.connectButton.addActionListener(this);

        this.setVisible(true);
    }


    /**
     * Processes button events
     */
    public void actionPerformed(ActionEvent e)
    {
        Object source = e.getSource();

        if (source == this.disconnectButton)
        {   
            //this.ldapService.disconnect();
        }
        else if (source == this.connectButton)
        { 
            int method;
            String bindDN = "";
            char[] password = {};
            //if(sslCheckBox.isSelected())
                //method = LdapService.LDAP_METHOD_SSL;
            //else
                //method = LdapService.LDAP_METHOD_SIMPLE;
            String hostname = this.hostnameTextField.getText();
            String port = this.portNumberTextField.getText();
            String baseDN = this.baseDNTextField.getText();
            if(anonymousCheckBox.isSelected() == false)
            {
                bindDN = this.bindDNTextField.getText();
                password = this.passwordField.getPassword();
            }
            //this.ldapService.connect(hostname, port, baseDN, method, bindDN, password);
        }
    }

    public void itemStateChanged(ItemEvent e) {

        Object source = e.getItemSelectable();

        if (source == this.sslCheckBox)
            if (e.getStateChange() == ItemEvent.DESELECTED)
                this.portNumberTextField.setText("389");
            else
                this.portNumberTextField.setText("636");
        else if (source == this.anonymousCheckBox)
        {
            this.bindDNTextField.setEnabled(e.getStateChange() == ItemEvent.DESELECTED);
            this.passwordField.setEnabled(e.getStateChange() == ItemEvent.DESELECTED);
        }
    }

    /**
     * Processes LDAP connection state change events.
     */
    public void ldapEventReceived(LdapEvent event)
    {
        /* that dirty implementation will be replaced
         * when i18n will be implemented
         */

        /* state messages */
        if(event.getNewState() == event.LDAP_STATE_DISCONNECTED)
        {
            this.connectionStatePane.setText(connectionStateText + "disconnected");
        }
        else if(event.getNewState() == event.LDAP_STATE_CONNECTED)
        {
            this.connectionStatePane.setText(connectionStateText + "connected");

        }
        else if(event.getNewState() == event.LDAP_STATE_DISCONNECTED)
        {

        }
        else if(event.getNewState() == event.LDAP_STATE_CONNECTING)
        {
            this.connectionStatePane.setText(connectionStateText + "connecting...");
        }
        else if(event.getNewState() == event.LDAP_STATE_DISCONNECTING)
        {
            this.connectionStatePane.setText(connectionStateText + "disconnecting...");
        }
        else
        {
            this.connectionStatePane.setText(connectionStateText + "unexpected state");
        }

        /* error messages */
        if(event.getError() == event.LDAP_ERROR_NONE)
        {
            this.connectionErrorPane.setText(connectionErrorText + "none");
        }
        else if(event.getError() == event.LDAP_ERROR_CONNECT)
        {
            this.connectionErrorPane.setText(connectionErrorText + "couldn't establish LDAP connection");
        }
        else if(event.getError() == event.LDAP_ERROR_AUTH)
        {
            this.connectionErrorPane.setText(connectionErrorText + "wrong credentials!");
        }
        else if(event.getError() == event.LDAP_INVALID_DN)
        {
            this.connectionErrorPane.setText(connectionErrorText + "invalid DN!");
        }
        else if(event.getError() == event.LDAP_NO_PASSWD)
        {
            this.connectionErrorPane.setText(connectionErrorText + "password needed");
        }
        else
        {
            this.connectionErrorPane.setText(connectionErrorText + "unknown error");
        }
    }


}
