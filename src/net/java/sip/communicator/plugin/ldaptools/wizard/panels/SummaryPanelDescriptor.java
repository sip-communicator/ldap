/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.plugin.ldaptools.wizard.panels;

import java.awt.*;
import javax.swing.*;

import net.java.sip.communicator.plugin.ldaptools.wizard.*;

public class SummaryPanelDescriptor
    extends WizardPanelDescriptor
{
    /**
     * unique identifier needed by the other panels and the wizard
     */
    public static final String IDENTIFIER = "SUMMARY_PANEL";

    /**
     * simple constructor
     */
    public SummaryPanelDescriptor()
    {
        super(IDENTIFIER, new SummaryPanel());
    }

    /**
     * Returns the panel to display when clicking the next button
     * @return the panel to display when clicking the next button
     */
    public Object getNextPanelDescriptor()
    {
        return null;
    }

    /**
     * Returns the panel to display when clicking the back button
     * @return the panel to display when clicking the back button
     */
    public Object getBackPanelDescriptor()
    {
        return SearchPanelDescriptor.IDENTIFIER;
    }  
}
