package net.java.sip.communicator.plugin.ldaptools.wizard.panels;

import java.awt.*;

public class Constants
{
    /**
     *      * The name of the font used in this ui implementation.
     *           */
    public static final String FONT_NAME = "Verdana";

    /**
     *      * The size of the font used in this ui implementation.
     *           */
    public static final String FONT_SIZE = "12";

    /**
     *      * The default <tt>Font</tt> object used through this ui implementation.
     *           */
    public static final Font FONT = new Font(Constants.FONT_NAME, Font.PLAIN,
            new Integer(Constants.FONT_SIZE).intValue());

}
