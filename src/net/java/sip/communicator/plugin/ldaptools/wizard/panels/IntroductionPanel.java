/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.plugin.ldaptools.wizard.panels;

import java.awt.*;
import java.net.*;
import javax.swing.*;
import javax.swing.border.*;

public class IntroductionPanel
    extends JPanel
{
    private JPanel contentPanel;

    /**
     * simple constructor
     */
    public IntroductionPanel()
    {
        this.contentPanel = this.getContentPanel();
        this.contentPanel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));

        this.setLayout(new BorderLayout());

        add(contentPanel, BorderLayout.NORTH);
    }
    
    /**
     * Fills the content panel and returns it
     * @return the filled content panel
     */
    private JPanel getContentPanel()
    {
        JPanel contentPanel1 = new JPanel();
        JPanel jPanel = new JPanel();
        JLabel jLabel = new JLabel();
        JLabel blankSpace = new JLabel();
        JLabel welcomeTitle = new JLabel();
        
        contentPanel1.setLayout(new BorderLayout());

        welcomeTitle.setFont(new Font("MS Sans Serif", Font.BOLD, 11));
        welcomeTitle.setText("Welcome to the Wizard Dialog!");
        contentPanel1.add(welcomeTitle, BorderLayout.NORTH);

        jPanel.setLayout(new java.awt.GridLayout(0, 1));

        jPanel.add(blankSpace);
        jLabel.setText("This wizard will help you to register an LDAP (Lightweight Directory Access Protocol) with SIP Communicator.");
        jPanel.add(jLabel);

        contentPanel1.add(jPanel, BorderLayout.CENTER);

        return contentPanel1;
    }
}
