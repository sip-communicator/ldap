/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.plugin.ldaptools.wizard.panels;

import java.awt.*;
import java.net.*;
import javax.swing.*;
import javax.swing.border.*;

public class AuthPanel
    extends JPanel
{
    private JPanel contentPanel;

    /**
     * simple constructor
     */
    public AuthPanel()
    {
        this.contentPanel = this.getContentPanel();
        this.contentPanel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));

        this.setLayout(new BorderLayout());

        add(contentPanel, BorderLayout.NORTH);
    }
    
    /**
     * Fills the content panel and returns it
     * @return the filled content panel
     */
    private JPanel getContentPanel()
    {
        JPanel mainPanel = new JPanel(new BorderLayout());
        JPanel titlePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 20));
        JPanel formPanel = new JPanel(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();

        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.WEST;

        /* title */
        JLabel title = new JLabel("Authentication settings");
        title.setFont(Constants.FONT.deriveFont(Font.BOLD, 18f));
        titlePanel.add(title);

        /* anonymous bind */
        JCheckBox anonymousCheckBox = new JCheckBox("Anonymous bind");
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 2;
        c.fill = GridBagConstraints.HORIZONTAL;
        formPanel.add(anonymousCheckBox, c);

        /* bind DN (Distinguished Name) */
        JLabel bindDNLabel = new JLabel("Bind DN:");
        JTextField bindDNField = new JTextField(25);
        bindDNLabel.setLabelFor(bindDNField);
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        formPanel.add(bindDNLabel, c);
        c.gridx = 1;
        c.gridy = 1;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        formPanel.add(bindDNField, c);

        /* password */
        JLabel passwordLabel = new JLabel("Password: ");
        JPasswordField passwordField = new JPasswordField(25);
        passwordLabel.setLabelFor(passwordField);
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        formPanel.add(passwordLabel, c);
        c.gridx = 1;
        c.gridy = 2;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        formPanel.add(passwordField, c);

        mainPanel.add(titlePanel, BorderLayout.NORTH);
        mainPanel.add(formPanel, BorderLayout.WEST);

        return mainPanel;
    }
}
