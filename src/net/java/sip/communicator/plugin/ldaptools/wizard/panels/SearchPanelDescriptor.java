/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.plugin.ldaptools.wizard.panels;

import java.awt.*;
import javax.swing.*;

import net.java.sip.communicator.plugin.ldaptools.wizard.*;

public class SearchPanelDescriptor
    extends WizardPanelDescriptor
{
    /**
     * unique identifier needed by the other panels and the wizard
     */
    public static final String IDENTIFIER = "Search_PANEL";

    /**
     * simple constructor
     */
    public SearchPanelDescriptor()
    {
        super(IDENTIFIER, new SearchPanel());
    }

    /**
     * Returns the panel to display when clicking the next button
     * @return the panel to display when clicking the next button
     */
    public Object getNextPanelDescriptor()
    {
        return SummaryPanelDescriptor.IDENTIFIER;
    }

    /**
     * Returns the panel to display when clicking the back button
     * @return the panel to display when clicking the back button
     */
    public Object getBackPanelDescriptor()
    {
        return AuthPanelDescriptor.IDENTIFIER;
    }  
}
