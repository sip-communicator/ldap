/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.plugin.ldaptools.wizard.panels;

import java.awt.*;
import java.awt.event.*;
import java.net.*;
import javax.swing.*;
import javax.swing.border.*;

public class ConnectionPanel
    extends JPanel
{
    private JPanel contentPanel;

    /* indexes in the combobox */
    public final static int SECURITY_CLEAR = 0;
    public final static int SECURITY_SSL = 1;

    /* components that old the important data */
    private JTextField hostnameField;
    private JComboBox securityList;
    private JSpinner portSpinner;
    private SpinnerNumberModel portModel;
    private JCheckBox defaultPortCheckBox;

    /**
     * simple constructor
     */
    public ConnectionPanel()
    {
        this.contentPanel = this.getContentPanel();
        this.contentPanel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));

        this.setLayout(new BorderLayout());

        add(contentPanel, BorderLayout.NORTH);
    }
    
    /**
     * Fills the content panel and returns it
     * @return the filled content panel
     */
    private JPanel getContentPanel()
    {
        JPanel mainPanel = new JPanel(new BorderLayout());
        JPanel titlePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 20));
        JPanel formPanel = new JPanel(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();

        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.WEST;

        /* title */
        JLabel title = new JLabel("Connection settings");
        title.setFont(Constants.FONT.deriveFont(Font.BOLD, 18f));
        titlePanel.add(title);

        /* hostname selection text field */
        JLabel hostnameLabel = new JLabel("Hostname: ");
        this.hostnameField = new JTextField(25);
        hostnameLabel.setLabelFor(hostnameField);
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        formPanel.add(hostnameLabel, c);
        c.gridx = 1;
        c.gridy = 0;
        c.gridwidth = 2;
        c.fill = GridBagConstraints.HORIZONTAL;
        formPanel.add(hostnameField, c);

        /* security: clear text, SSL */
        String[] securityStrings = { "clear text", "SSL" };
        this.securityList = new JComboBox(securityStrings);
        securityList.setSelectedIndex(0);
        JLabel securityLabel = new JLabel("Connection security: ");
        securityLabel.setLabelFor(securityList);
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        formPanel.add(securityLabel, c);
        c.gridx = 1;
        c.gridy = 1;
        c.gridwidth = 2;
        c.fill = GridBagConstraints.HORIZONTAL;
        formPanel.add(securityList, c);

        /* network port number */
        JLabel portLabel = new JLabel("Port: ");
        this.portModel = new SpinnerNumberModel(389, 1, 65535,1);
        this.portSpinner = new JSpinner(portModel);
        portLabel.setLabelFor(portSpinner);
        this.defaultPortCheckBox = new JCheckBox("Use default");
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        formPanel.add(portLabel, c);
        c.gridx = 1;
        c.gridy = 2;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.NONE;
        formPanel.add(portSpinner, c);
        c.gridx = 2;
        c.gridy = 2;
        c.gridwidth = 1;
        c.fill = GridBagConstraints.NONE;
        formPanel.add(defaultPortCheckBox, c);

        mainPanel.add(titlePanel, BorderLayout.NORTH);
        mainPanel.add(formPanel, BorderLayout.WEST);

        return mainPanel;
    }

    void addDefaultPortActionListener(ActionListener l)
    {
        this.defaultPortCheckBox.addActionListener(l);
    }

    public void addSecurityActionListener(ActionListener l)
    {
        this.securityList.addActionListener(l);
    }

    public boolean isDefaultPortSelected()
    {
        return this.defaultPortCheckBox.isSelected();
    }

    public void setDefaultPortSelected(boolean state)
    {
        this.defaultPortCheckBox.setSelected(state);
    }

    public String getHostname()
    {
        return this.hostnameField.getText();
    }

    public void setHostname(String hostname)
    {
        this.hostnameField.setText(hostname);
    }

    public int getSecurity()
    {
        return this.securityList.getSelectedIndex();
    }

    public void setSecurity(int security)
    {
        this.securityList.setSelectedIndex(security);
    }

    public int getPort()
    {
        return this.portModel.getNumber().intValue();
    }

    public void setPort(int port)
    {
        this.portModel.setValue(new Integer(port));
    }

    public void setPortEnabled(boolean state)
    {
        this.portSpinner.setEnabled(state);
    }

}
