
/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.plugin.ldaptools.wizard.panels;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import net.java.sip.communicator.plugin.ldaptools.wizard.*;

public class ConnectionPanelDescriptor
    extends WizardPanelDescriptor
    implements ActionListener
{
    /**
     * unique identifier needed by the other panels and the wizard
     */
    public static final String IDENTIFIER = "CONNECTION_PANEL";

    private final static int CLEAR_PORT = 329;
    private final static int SSL_PORT = 636;

    private ConnectionPanel panel = new ConnectionPanel();

    /**
     * simple constructor
     */
    public ConnectionPanelDescriptor()
    {
        panel.addDefaultPortActionListener(this);
        panel.addSecurityActionListener(this);
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(panel);
    }

    /**
     * Returns the panel to display when clicking the next button
     * @return the panel to display when clicking the next button
     */
    public Object getNextPanelDescriptor()
    {
        return AuthPanelDescriptor.IDENTIFIER;
    }

    /**
     * Returns the panel to display when clicking the back button
     * @return the panel to display when clicking the back button
     */
    public Object getBackPanelDescriptor()
    {
        return IntroductionPanelDescriptor.IDENTIFIER;
    }  

    public void actionPerformed(ActionEvent e)
    {
        setPortAccordingToCheckBox();
        setPortAccordingToSecurity();
    }

    private void setPortAccordingToCheckBox()
    {
        panel.setPortEnabled(!panel.isDefaultPortSelected());
    }

    /**
     * changes the port number according to security setting (ssl)
     * if the user chose to stick with default port value
     */
    private void setPortAccordingToSecurity()
    {
        if(panel.isDefaultPortSelected())
        {
            if(panel.getSecurity() == ConnectionPanel.SECURITY_SSL)
            {
                panel.setPort(SSL_PORT);
            }
            if(panel.getSecurity() == ConnectionPanel.SECURITY_CLEAR)
            {
                panel.setPort(CLEAR_PORT);
            }
        }
    }
}
