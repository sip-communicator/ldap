/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.service.ldap;

import java.util.*;

import net.java.sip.communicator.service.ldap.event.*;

/**
 * An LdapServer stores settings for one directory server
 *
 * @author Sébastien Mazy
 */
public interface LdapServer
    extends LdapConstants,
            Comparable<LdapServer>
{

    public String getName();

    /**
     * @return the hostname used to connect to the directory
     */
    public String getHostname();

    /**
     * @return the TCP port used to connect to the directory
     */
    public int getPort();

    /**
     * @return the distinguished name used as a base for searches
     */
    public String getBaseDN();

    /**
     * @return the distinguished name used to connect to the directory
     */
    public String getBindDN();

    /**
     * @return the password used to connect to the directory
     */
    public String getPassword();

    /**
     * @return an int taken from one of the LDAP_METHOD_* constants
     * representing the connection method used to connect to the directory
     */
    public ConnectMethod getConnectMethod();

    /**
     * @return the printable name chosen to name this server
     */
    public String toString();

    public void searchContact(String query);

    public void addLdapListener(LdapListener listener);

}
