/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.service.ldap;

import java.util.*;

/**
 * The Ldap Service allows other modules to query an LDAP server
 *
 * @author Sébastien Mazy
 */
public interface LdapService
    extends LdapConstants
{


    /**
     * Returns all the LDAP directories
     *
     * @return a collection containing all the LdapServer
     */
    public LdapServerSet getServerSet();

    /**
     * @param name name of this server settings in the UI
     * @param hostname hostname of the remote directory
     * @param port port TCP port of the remote directory
     * @param baseDN distinguished name used as a search base
     * @param bindDN distinguished name used to bind to the remote directory
     * @param password password used to bind to the remote directory
     * @param connectMethod method used to connect to the remote directory
     * Should match one of LDAP_METHOD_* constants
     */
    public LdapServer createLdapServer(
            String name,
            String hostname,
            int port,
            String baseDN,
            String bindDN,
            String password,
            ConnectMethod method
            )
        throws IllegalArgumentException;

    /**
     * @param name name of this server settings in the UI
     * @param hostname hostname of the remote directory
     * @param port port TCP port of the remote directory
     * @param baseDN distinguished name used as a search base
     * @param connectMethod method used to connect to the remote directory
     * Should match one of LDAP_METHOD_* constants
     */
    public LdapServer createLdapServer(
            String name,
            String hostname,
            int port,
            String baseDN,
            ConnectMethod method
            )
        throws IllegalArgumentException;
}
