/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.service.ldap.event;

import java.util.*;


/**
 * An LdapEvent is triggered when 
 * the state of the LDAP connection changes.
 *
 * @author Sébastien Mazy
 */
public interface LdapListener
    extends EventListener
{
    /**
     * This method gets called when
     * the state of the LDAP connection changes.
     *
     * @param event An LdapStateChangeEvent showing
     * previous and new states of the LDAP connection.
     */
    public void ldapEventReceived(LdapEvent event);

}
