/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.service.ldap.event;

import java.util.*;

import net.java.sip.communicator.service.ldap.*;


/**
 * An LdapEvent is triggered when
 * the state of the LDAP connection changes.
 * Available states for the moment are : 
 * connected
 * disconnected
 * connecting
 * disconnecting
 *
 * @author Sébastien Mazy
 */
public class LdapEvent
    extends EventObject
{

    public static enum LdapEventCause { NEW_SEARCH_RESULT, SEARCH_ACHIEVED }

    /**
     * An LDAP state value indicating
     * a disconnected state.
     */
    public static final int LDAP_STATE_DISCONNECTED = 0;

    /**
     * An LDAP state value indicating
     * a connected state.
     */
    public static final int LDAP_STATE_CONNECTED = 1;

    /**
     * An LDAP state value indicating
     * a connection pending state.
     */
    public static final int LDAP_STATE_CONNECTING = 2;

    /**
     * An LDAP state value indicating
     * a disconnection pending state.
     */
    public static final int LDAP_STATE_DISCONNECTING = 3;

    /**
     * An LDAP error value indicating
     * no error ;).
     */
    public static final int LDAP_ERROR_NONE = 0;

    /**
     * An LDAP error value indicating an error
     * while establishing the LDAP connection
     * (ex: couln't resolve hostname).
     */
    public static final int LDAP_ERROR_CONNECT = 1;

    /**
     * An LDAP error value indicating an error
     * while authenticating (ex: bad credentials).
     */
    public static final int LDAP_ERROR_AUTH = 2;

    /**
     * An LDAP error value indicating an error in the
     * distinguished name syntax used for authentication.
     */
    public static final int LDAP_INVALID_DN = 3;

    /**
     * An LDAP error value indicating that not password
     * was provided with the distinguished name for authentication.
     */
    public static final int LDAP_NO_PASSWD = 4;

    /**
     * An LDAP error value indicating an unknown error
     */
    public static final int LDAP_ERROR_UNKNOWN = 5;

    /**
     * New state for LDAP connection. 
     */
    private int newState;

    /**
     * Previous state for LDAP connection.
     */
    private int previousState;

    /**
     * Was the state change triggered by an error ?
     */
    private int error;

    private final LdapEventCause cause;

    private final Object content;


    public LdapEvent(LdapServer source, LdapEventCause cause)
    {
        this(source, cause, null);
    }


    public LdapEvent(LdapServer source, LdapEventCause cause, Object content)
    {
        super(source);
        this.cause = cause;
        this.content = content;
    }

    /**
     * Get the previous state of the LDAP connection.
     *
     * @return the previous state of the LDAP connection.
     */
    public int getPreviousState()
    {
        return this.previousState;
    }

    /**
     * Get the new state of the LDAP connection.
     *
     * @return the new state of the LDAP connection.
     */
    public int getNewState()
    {
        return this.newState;
    }

    /**
     * Get the new state of the LDAP connection.
     *
     * @return the new state of the LDAP connection.
     */
    public int getError()
    {
        return this.error;
    }

    public LdapEventCause getCause()
    {
        return this.cause;
    }

    public Object getContent()
    {
        return this.content;
    }

}
