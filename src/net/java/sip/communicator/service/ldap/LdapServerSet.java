/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.service.ldap;

import java.util.*;

/**
 * The LdapServerSet is actually a sorted map
 * linking server aliases (ie displayed name)
 * to LdapServer objects. It store all the
 * servers registered in the configuration and
 * provides methods that address them all.
 *
 * @author Sébastien Mazy
 */
public interface LdapServerSet
    extends Iterable<LdapServer>
{
    /**
     * Tries to get the LdapServer whose alias is given or return null
     * if it is absent of the LdapServerSet
     *
     * @param  alias alias of the wanted Ldapserver
     * @return the asked LdapServer or null if absent
     */
    //public LdapServer get(String alias);

    /**
     * Tries to create and add an LdapServer to the LdapServerSet.
     *
     * @param alias name of this server settings in the UI
     * @param hostname hostname of the remote directory
     * @param port port TCP port of the remote directory
     * @param baseDN distinguished name used as a search base
     * @param bindDN distinguished name used to bind to the remote directory
     * @param password password used to bind to the remote directory
     * @param connectMethod method used to connect to the remote directory
     */
    //public void add(String alias, String hostname, int port, String baseDN,
    //        String bindDN, char[] password, int connectMethod);

    /**
     * Tries to remove the server whose alias is given from the LdapServerSet.
     *
     * @param alias Alias of the LdapServer to remove
     */
    //public void remove(String alias);

    /**
     * Returns a snapshot view of the servers. The LdapServer-s are sorted
     * lexicographically by their alias. The returned Vector is *not* backed
     * by the LdapServerSet, so changes to the Collection will *not* be
     * reflected in the LdapServerSet. This method is mainly meant for
     * displaying the list of LdapServer-s in the UI.
     *
     * @return a snapshot view of the servers, sorted by alias
     */
    //public Vector<LdapServer> getServers();

    /**
     * Returns an obserable object. Use it to get notified from any change in
     * the LdapServerSet or in any of its LdapServer-s.
     *
     * @return an Observable notifying any change in the LdapServerSet
     */
    //public Observable getObservable();
    
    public LdapServer getServerWithName(String name);

    public LdapServer removeServerWithName(String name);

    public boolean addServer(LdapServer server);

    public boolean containsServerWithName(String name);

    public int size();
}
