/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.service.ldap;

/**
 * Constants used by this LDAP service implementation
 *
 * @author Sébastien Mazy
 */
public interface LdapConstants
{
    public static enum ConnectMethod{SIMPLE, SSL, SASL}
    /**
     * How long should we wait for a LDAP response ?
     * (in ms)
     */
    public static final String LDAP_TIMEOUT
        = "5000";

    /**
     * this constant reprensents clear text used as a connection method
     */
    //public static final int LDAP_METHOD_SIMPLE = 0;

    /**
     * this constant represents SSL used as a connection method
     */
    //public static final int LDAP_METHOD_SSL = 1;

    /**
     * this constant represents SASL used as a connection method 
     * (SASL stands for Simple Authentication and Security Layer)
     */
    //public static final int LDAP_METHOD_SASL = 2;
    /* TODO SASL is too vague, maybe we should provide more constants to describe it */

    /* TODO more connect methods */


}
