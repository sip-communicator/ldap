package net.java.sip.communicator.service.ldap;

import java.util.*;

public interface LdapSearchResult
    extends Iterable<LdapSearchResult.ResultEntry>
{
    public String getDisplayedName();

    public void addEntry(String protocol, String LDAPName, String address);

    public interface ResultEntry {
        public String getProtocol();
        public String getLdapName();
        public String getAddress();
    }
}
