/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.impl.ldap;

import java.io.*;
import java.util.*;

import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.event.*;
import java.util.concurrent.*; /* ConcurrentSkipListSet */
/* import javax.naming.ldap.*; not currently needed */

import org.osgi.framework.*;
import net.java.sip.communicator.service.ldap.*;
import net.java.sip.communicator.service.ldap.event.*;
import net.java.sip.communicator.util.*;

/**
 * The LDAP service allows other modules to query an LDAP server.
 *
 * @author Sébastien Mazy
 */
public class LdapServiceImpl
    implements  LdapService
{
    /**
     * All the servers registered
     */
    private LdapServerSet serverSet
        = new LdapServerSetImpl();

    /**
     * The logger for this class.
     */
    private static Logger logger = Logger
        .getLogger(LdapServiceImpl.class);

    /**
     * BundleContext from the OSGI bus.
     */
    private BundleContext bundleContext;

    /**
     * Starts the service.
     *
     * @param bc BundleContext
     */
    public void start(BundleContext bc)
    {
        this.logger.trace("Starting the LDAP implementation.");
        this.bundleContext = bc;
    }

    /**
     * Stops the service.
     *
     * @param bc BundleContext
     */
    public void stop(BundleContext bc)
    {
        this.logger.trace("Stopping the LDAP implementation.");
    }


    /**
     * @see net.java.sip.communicator.service.ldap#getServers
     */
    public LdapServerSet getServerSet()
    {
        return this.serverSet;
    }

    public LdapServer createLdapServer(String name, String hostname, int port,
            String baseDN, String bindDN, String password,
            ConnectMethod method)
        throws IllegalArgumentException
    {
        return new LdapServerImpl(name, hostname, port, baseDN, bindDN,
                password, method);
    }

    public LdapServer createLdapServer(String name, String hostname, int port,
            String baseDN, ConnectMethod method)
        throws IllegalArgumentException
    {
        return new LdapServerImpl(name, hostname, port, baseDN, method);
    }
}
