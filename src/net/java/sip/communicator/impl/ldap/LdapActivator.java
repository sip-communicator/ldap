/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.impl.ldap;

import org.osgi.framework.*;
import net.java.sip.communicator.util.*;
import net.java.sip.communicator.service.ldap.*;

/**
 * Activates the LdapService
 *
 * @author Sébastien Mazy
 */
public class LdapActivator
    implements BundleActivator
{

    private static Logger logger =
        Logger.getLogger(LdapActivator.class);

    private LdapServiceImpl ldapService = null;

    /**
     * Starts the LDAP service
     *
     * @param bundleContext BundleContext
     * @throws Exception
     */
    public void start(BundleContext bundleContext)
        throws Exception
    {
        try{

            this.logger.logEntry();

            /* Creates and starts the LDAP service. */
            this.ldapService =
                new LdapServiceImpl();

            this.ldapService.start(bundleContext);

            bundleContext.registerService(
                    LdapService.class.getName(), this.ldapService, null);

            this.logger.trace("LDAP Service ...[REGISTERED]");
        }
        finally
        {
            this.logger.logExit();
        }

    }

    public void stop(BundleContext bundleContext)
        throws Exception
    {
        if(this.ldapService != null)
            this.ldapService.stop(bundleContext);
    }
}
