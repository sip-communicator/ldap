package net.java.sip.communicator.impl.ldap;

import java.util.*;

import net.java.sip.communicator.service.protocol.*;

import net.java.sip.communicator.service.ldap.*;

public class LdapSearchResultImpl
    implements LdapSearchResult
{
    private final String displayedName;

    private Collection<LdapSearchResult.ResultEntry> entries =
        new Vector<LdapSearchResult.ResultEntry>();

    public LdapSearchResultImpl(String displayedName)
    {
        if(displayedName == null)
            throw new NullPointerException();
        this.displayedName = displayedName;
    }

    public String getDisplayedName()
    {
        return this.displayedName;
    }

    public String toString()
    {
        StringBuffer output = new StringBuffer();
        output.append(this.getDisplayedName());
        output.append(" has addresses :\n");

        for(LdapSearchResult.ResultEntry entry : this.entries)
        {
            output.append(entry.getProtocol());
            output.append(": ");
            output.append(entry.getAddress());
            output.append(" (LDAP name: ");
            output.append(entry.getLdapName());
            output.append(")\n");
        }

        return output.toString();
    }

    public void addEntry(String protocol, String ldapName, String address)
    {
        this.entries.add(new ResultEntryImpl(protocol, ldapName, address));
    }

    public class ResultEntryImpl
            implements LdapSearchResult.ResultEntry
    {
        private final String protocol;
        private final String ldapName;
        private final String address;

        public ResultEntryImpl(String protocol, String ldapName, String address)
        {
            if(protocol == null || ldapName == null || address == null)
                    throw new NullPointerException();
            if(!protocol.equals(ProtocolNames.JABBER) && !protocol.equals(ProtocolNames.SIP))
                this.protocol = protocol;
            else
                throw new IllegalArgumentException("Jabber and SIP are the only supported protocols!");
            this.ldapName = ldapName;
            this.address = address;
        }

        public String getProtocol()
        {
            return this.protocol;
        }

        public String getLdapName()
        {
            return this.ldapName;
        }

        public String getAddress()
        {
            return this.address;
        }
    }

    public Iterator<LdapSearchResult.ResultEntry> iterator()
    {
        return entries.iterator();
    }
}
