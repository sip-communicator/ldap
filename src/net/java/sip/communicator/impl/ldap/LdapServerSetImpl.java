/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.impl.ldap;

import java.lang.*;
import java.util.*;
import java.util.concurrent.*;

import net.java.sip.communicator.service.ldap.*;

/**
 * A thread-safe implementation of LdapServerSet, backed by a TreeMap
 *
 * @see net.java.sip.communicator.service.ldap
 *
 * @author Sébastien Mazy
 */

public class LdapServerSetImpl
    implements LdapServerSet
{

    private SortedMap<String,LdapServer> serverMap;

    //private InnerObservable innerObservable = new InnerObservable();
    



    /**
     * @see java.util.Observer#update
     * 
     */
    /*
    public void update(Observable o, Object arg)
    {
        this.innerObservable.setChangedAndNotify();
    }
    */

    /**
     * @see net.java.sip.communicator.service.ldap.LdapServerSet#getObservable
     */
    /*
    public Observable getObservable()
    {
        return this.innerObservable;
    }
    */

    /**
     * inner class InnerObservable used to create our
     * embedded Observable object
     */
    /*
    private class InnerObservable extends Observable
    {
        public void setChangedAndNotify()
        {
            this.setChanged();
            this.notifyObservers();
        }
    }
    */

    public LdapServerSetImpl()
    {
        this.serverMap = Collections.synchronizedSortedMap(new TreeMap<String, LdapServer>());
    }

    public LdapServer getServerWithName(String name)
    {
        return this.serverMap.get(name);
    }

    public LdapServer removeServerWithName(String name)
    {
        return this.serverMap.remove(name);
    }

    public boolean addServer(LdapServer server)
    {
        if(!this.containsServerWithName(server.getName()))
        {
            this.serverMap.put(server.getName(), server);
            return true;
        }
        return false;
    }

    public boolean containsServerWithName(String name)
    {
        return this.serverMap.containsKey(name);
    }

    public Iterator<LdapServer> iterator()
    {
        return this.serverMap.values().iterator();
    }

    public int size()
    {
        return this.serverMap.size();
    }
}
