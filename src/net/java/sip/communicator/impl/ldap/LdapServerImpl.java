/*
 * SIP Communicator, the OpenSource Java VoIP and Instant Messaging client.
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package net.java.sip.communicator.impl.ldap;

import java.util.*;
import java.util.regex.*;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.event.*;

import net.java.sip.communicator.util.*;
import net.java.sip.communicator.service.protocol.*;

import net.java.sip.communicator.service.ldap.*;
import net.java.sip.communicator.service.ldap.event.*;

/**
 * An LdapServer stores settings for one directory server
 *
 * @author Sébastien Mazy
 */
public class LdapServerImpl
    implements LdapServer
{

    private final static Logger logger = Logger
        .getLogger(LdapServiceImpl.class);

    /**
     * name that will be displayed in the UI
     * e.g. "My LDAP server"
     */
    private final String name;

    private final Vector<String> searchableAttributes = new Vector<String>();

    private final HashMap<String, List<String>> retrievableAttributes
        = new HashMap<String, List<String>>();

    /**
     * distinguished name used as a base for searches
     * e.g. "dc=example,dc=com"
     */
    private final String baseDN;

    private final Hashtable<String, String> env = new Hashtable<String, String>();

    SearchControls searchControls = new SearchControls(); 

    /**
     * All property change listeners registered so far.
     */
    private Vector<LdapListener> ldapListeners = new Vector<LdapListener>();

    public LdapServerImpl(
            String name,
            String hostname,
            int port,
            String baseDN,
            ConnectMethod method
            )
        throws IllegalArgumentException
    {
        if(!textHasContent(name))
        {
            throw new IllegalArgumentException("name has no content.");
        }
        if(!textHasContent(hostname))
        {
            throw new IllegalArgumentException("Hostname has no content.");
        }
        if(!textHasContent(baseDN))
        {
            throw new IllegalArgumentException("Base DN has no content.");
        }
        if(port < 0 || port > 65535)
        {
            throw new IllegalArgumentException("Illegal port number.");
        }
        this.name = name;
        this.baseDN = baseDN;

        String portText;
        if(port == 0)
            portText = "";
        portText = ":" + port;

        this.env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        this.env.put("com.sun.jndi.ldap.connect.timeout", LDAP_TIMEOUT);
        this.env.put("com.sun.jndi.ldap.read.timeout", LDAP_TIMEOUT);

        if(method == ConnectMethod.SSL)
        {
            this.env.put(Context.SECURITY_AUTHENTICATION, "SSL"); 
            this.env.put(Context.PROVIDER_URL, "ldaps://" + hostname + portText+"/"); 
        }
        else if(method == ConnectMethod.SASL)
        {
            this.env.put(Context.SECURITY_AUTHENTICATION, "SASL"); 
            throw new IllegalArgumentException("this implementation does not support SALS auth mechanism for now.");
        }
        else
        {
            this.env.put(Context.PROVIDER_URL, "ldap://" + hostname + portText+"/"); 
            this.env.put(Context.SECURITY_AUTHENTICATION, "simple"); 
        }

        this.searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE); 
        this.searchControls.setCountLimit(100);

        this.searchableAttributes.add("cn");
        this.searchableAttributes.add("sn");
        this.searchableAttributes.add("givenname");
        this.searchableAttributes.add("name");
        this.searchableAttributes.add("uid");

        /* attributes that are not SIP/IM addresses
         * but which we want to display in the UI
         * e.g. "cn" (common name) */
        List<String> otherAttributes = new Vector<String>();
        otherAttributes.add("mail");
        this.retrievableAttributes.put(ProtocolNames.JABBER, otherAttributes);

        /* attributes that may contain a jabber address */
        List<String> jabberAttributes = new Vector<String>();
        jabberAttributes.add("mail");
        this.retrievableAttributes.put(ProtocolNames.JABBER, jabberAttributes);
    
        /* attributes that may contain a SIP address */
        List<String> SIPAttributes = new Vector<String>();
        SIPAttributes.add("Phone");
        SIPAttributes.add("companyPhone");
        SIPAttributes.add("telephoneNumber");
        this.retrievableAttributes.put(ProtocolNames.SIP, SIPAttributes);
    }

    /**
     * Constructor for this class
     *
     * @param name name of this server settings in the UI, e.g "my server"
     * @param hostname hostname of the remote directory, e.g. "example.com"
     * @param port port TCP port of the remote directory, e.g 389
     * @param baseDN distinguished name used as a search base, e.g. "dc=example, dc=com"
     * @param bindDN distinguished name used to bind to the remote directory, e.g. "cn=admin,cn=example,dc=com"
     * @param password password used to bind to the remote directory, e.g. "secret"
     * @param connectMethod method used to connect to the remote directory, e.g. LdapConstants.ConnectMethod.SSL
     *
     * @see net.java.sip.communicator.service.ldap.LdapConstants#ConnectMethod
     */
    public LdapServerImpl(String name, String hostname, int port, String baseDN,
            String bindDN, String password, ConnectMethod method)
    {
        this(name, hostname, port, baseDN, method);
        if(!textHasContent(bindDN))
        {
            throw new IllegalArgumentException("Bind DN has no content.");
        }
        if(password == null)
        {
            throw new IllegalArgumentException("Password is null");
        }
        this.env.put(Context.SECURITY_PRINCIPAL, bindDN); 
        this.env.put(Context.SECURITY_CREDENTIALS, password); 
    }

    /**
     * @see net.java.sip.communicator.service.ldap.LdapServer#getHostName
     */
    public String getHostname()
    {
        Pattern p = Pattern.compile("^ldaps?://(.*?)(:(\\d+?))?(/.*)?$");
        Matcher m = p.matcher(env.get(Context.PROVIDER_URL));
        if(m.matches())
        {
            /* debug */
            for(int i = 0 ; i <= m.groupCount() ; i++)
               logger.trace("group " + i + ": \"" + m.group(i) + "\"");
            return m.group(1);
        }
        return "localhost";
    }

    /**
     * @see net.java.sip.communicator.service.ldap.LdapServer#getPort
     */
    public int getPort()
    {
        Pattern p = Pattern.compile("^ldaps?://.*?(:(\\d+?))(/.*)?$");
        Matcher m = p.matcher(env.get(Context.PROVIDER_URL));
        if(m.matches())
        {
            /* debug */
            for(int i = 0 ; i <= m.groupCount() ; i++)
                logger.trace("group " + i + ": \"" + m.group(i) + "\"");
            return Integer.parseInt(m.group(2));
        }
        return 0;
    }

    /**
     * @see net.java.sip.communicator.service.ldap.LdapServer#getBaseDN
     */
    public String getBaseDN()
    {
        return this.baseDN;
    }

    /**
     * @see net.java.sip.communicator.service.ldap.LdapServer#getBindDN
     */
    public String getBindDN()
    {
        return this.env.get(Context.SECURITY_PRINCIPAL);
    }

    /**
     * @see net.java.sip.communicator.service.ldap.LdapServer#getPassword
     */
    public String getPassword()
    {
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put("com.sun.jndi.ldap.connect.timeout", LDAP_TIMEOUT);
        env.put("com.sun.jndi.ldap.read.timeout", LDAP_TIMEOUT);

        return this.env.get(Context.SECURITY_CREDENTIALS);
    }

    /**
     * @see net.java.sip.communicator.service.ldap.LdapServer#getConnectMethod
     */
    public ConnectMethod getConnectMethod()
    {
        String method = (String) this.env.get(Context.SECURITY_AUTHENTICATION);
        if(method.equals("SSL"))
            return ConnectMethod.SSL;
        else if(method.equals("SASL"))
            return ConnectMethod.SASL; 
        return ConnectMethod.SIMPLE;
    }

    /**
     * @return the name associated to this LdapServer
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * @see net.java.sip.communicator.service.ldap.LdapServer#toString
     */
    public String toString()
    {
        return this.getName().toString();
    }

    public int compareTo(LdapServer server)
    {
        return this.name.compareTo(server.getName());
    }

    public boolean equals(Object anObject)
    {
        if(anObject instanceof LdapServer)
        {
            LdapServer anLdapServer = (LdapServer) anObject;
            return this.name.equals(anLdapServer.getName());
        }
        return false;
    }

    public int hashCode()
    {
        return this.name.hashCode();
    }

    private boolean textHasContent(String aText)
    {
        String EMPTY_STRING = "";
        return (aText != null) && (!aText.trim().equals(EMPTY_STRING));
    }

    public void searchContact(final String query)
    {
        new Thread() {
            public void run()
            {
                DirContext dirContext; 
                logger.trace("starting search..."); 
                try { 
                    dirContext = new InitialDirContext(env); 
                    NamingEnumeration result = dirContext.search(baseDN, buildSearchFilter(query), searchControls);
                    while (result.hasMore()) { 
                        SearchResult sr = (SearchResult)result.next(); 
                        LdapSearchResult ldapResult = new LdapSearchResultImpl((String) sr.getAttributes().get("cn").get());
                        ldapResult.addEntry("jabber", "mail", (String) sr.getAttributes().get("mail").get());
                        LdapEvent event = new LdapEvent(LdapServerImpl.this, LdapEvent.LdapEventCause.NEW_SEARCH_RESULT, (Object) ldapResult);
                        fireLdapEvent(event);
                    } 
                    dirContext.close(); 
                } catch (NamingException e) { 
                    System.err.println("Erreur lors de l'acces au serveur LDAP" + e); 
                    e.printStackTrace(); 
                } 
                LdapEvent event = new LdapEvent(LdapServerImpl.this, LdapEvent.LdapEventCause.SEARCH_ACHIEVED);
                fireLdapEvent(event);
            }
        }.start();
    } 

    private String buildSearchFilter(String query)
    {
        StringBuffer searchFilter = new StringBuffer();
        searchFilter.append("(&(|");

        /* mail=* OR telephoneNumber=* OR ... */
        for(List<String> list : this.retrievableAttributes.values())
        {
            for(String attribute : list)
            {
                searchFilter.append("(");
                searchFilter.append(attribute);
                searchFilter.append("=*)");
            }
        }

        searchFilter.append(")(|");

        /* cn=*query* OR sn=*query* OR ... */
        for(String attribute : this.searchableAttributes)
        {
            searchFilter.append("(");
            searchFilter.append(attribute);
            searchFilter.append("=*)");
        }

        searchFilter.append("))");

        this.logger.trace(searchFilter.toString());

        return searchFilter.toString();
    }

    /**
     * Adds a LdapListener to the listener list.
     *
     * @param listener  The LdapListener to be added
     */
    public synchronized void addLdapListener(
            LdapListener listener)
    {
        this.ldapListeners.addElement(listener);
    }

    /**
     * Removes a LdapListener from the listener list.
     *
     * @param listener The LdapListener to be removed
     */
    protected synchronized void removeLdapListener(
            LdapListener listener)
    {
        this.ldapListeners.removeElement(listener);
    }


    /**
     * Fires an existing LdapEvent to any registered listeners.
     * @param event  The LdapEvent object.
     */
    private void fireLdapEvent(LdapEvent event)
    {
        for(LdapListener listener : this.ldapListeners)
        {
            listener.ldapEventReceived(event);
        }
    }

}
